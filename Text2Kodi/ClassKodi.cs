﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Text2Kodi;
using Text2Kodi.Properties;

namespace VideoLinks.ClassHelper
{
    internal class ClassKodi
    {
        private static readonly HttpClient _Client = new HttpClient();

        /// <summary>
        /// play or append to kodi queue
        /// </summary>
        /// <param name="jLink"></param>
        /// <param name="queue">true append</param>

        public static async void SendText2Kodi(string jText, bool queue = false, bool autoqueue = false)
        {
            await Run3(jText);
        }



        public static async Task<bool> Run3(string link)
        {
            string kodiIP = Settings.Default.rpi;
            string kodiUser = Settings.Default.username.Trim();
            string kodiPort = Settings.Default.port;
            //  string kodiPass = Properties.Settings.Default.password; https://stackoverflow.com/questions/12657792/how-to-securely-save-username-password-local
            byte[]? plaintext = null;
            string kodiPass = "";


            if (Settings.Default.cipher != null && Settings.Default.entropy != null)
            {
                plaintext = ProtectedData.Unprotect(Settings.Default.cipher, Settings.Default.entropy,
                                                    DataProtectionScope.CurrentUser);
                kodiPass = ByteArrayToString(plaintext);
            }

            kodiPass = kodiPass.Trim();

            string values = kodiUser + ":" + kodiPass;
            string kodiurl = "http://" + kodiIP + ":" + kodiPort + "/jsonrpc?request=";

                link = "{ \"id\":0,\"jsonrpc\":\"2.0\",\"method\":\"Input.SendText\",\"params\":{ \"text\":\"" + link + "\",\"done\":true}}";

                try
                {
                    var response = await Request(HttpMethod.Post, kodiurl, link, values);
                    string responseText = await response.Content.ReadAsStringAsync();

                    if (responseText.Contains("OK") /*&& link.Contains("Playlist.Add")*/)
                    {
                        NotificationBox.Show("Send to Kodi: OK", 1300, NotificationMsg.OK);

#if DEBUG
                        MessageBox.Show(responseText);
                        Console.WriteLine(responseText);
                        Console.ReadLine();
#endif
                    }
                    else if (responseText.Contains("error") /*&& link.Contains("Playlist.Add")*/)
                    {
                        NotificationBox.Show("Kodi ERROR", 1300, NotificationMsg.ERROR);
#if DEBUG
                        MessageBox.Show(responseText);
                        Console.WriteLine(responseText);
                        Console.ReadLine();
#endif

                        return false;
                    }

                    kodiPass = "";  //to be safe
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Kodi not responding. " + ex.Message, "Play", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            return true;
        }



        /// <summary>
        /// Makes an async HTTP Request
        /// </summary>
        /// <param name="pMethod">Those methods you know: GET, POST, HEAD, etc...</param>
        /// <param name="pUrl">Very predictable...</param>
        /// <param name="pJsonContent">String data to POST on the server</param>
        /// <param name="pHeaders">If you use some kind of Authorization you should use this</param>
        /// <returns></returns>
        private static async Task<HttpResponseMessage> Request(HttpMethod pMethod, string pUrl, string pJsonContent, string values/*Dictionary<string, string> pHeaders*/)
        {
            var httpRequestMessage = new HttpRequestMessage();

            httpRequestMessage.Method = pMethod;
            httpRequestMessage.RequestUri = new Uri(pUrl);

            //foreach (var head in pHeaders)
            //{
            //  //  httpRequestMessage.Headers.Add(head.Key, head.Value);

            //}

            var byteArray = Encoding.ASCII.GetBytes(values/*"my_client_id:my_client_secret"*/);
            var header = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            httpRequestMessage.Headers.Authorization = header;

            switch (pMethod.Method)
            {
                case "POST":
                    HttpContent? httpContent = new StringContent(pJsonContent, Encoding.UTF8, "application/json");

                    httpRequestMessage.Content = httpContent;
                    break;
            }

            return await _Client.SendAsync(httpRequestMessage);
        }




        private static void MouseWait()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                Mouse.OverrideCursor = Cursors.Wait;
            });

        }

        private static void MouseNull()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                Mouse.OverrideCursor = null;
            });

        }

        /// <summary>
        /// byte to string / string to byte
        /// </summary>
        /// <param name="arr"></param>
        /// <returns>string</returns>
        public static string ByteArrayToString(byte[] arr)
        {
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            return enc.GetString(arr);
        }



    }
}
