﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using VideoLinks.ClassHelper;

namespace Text2Kodi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            mainText2Kodi.Title = String.Format("Text2kodi" + " v{0}",
            Assembly.GetEntryAssembly().GetName().Version.ToString().Substring(0, 6).Trim('.'));

        }

        private void txtSendText_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                btnSend.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));

            if (e.Key == Key.Escape)
                this.Close();

        }

        private void btnSend_Click(object sender, RoutedEventArgs e)
        {
            var s_sendText = txtSearchString.Text;
            ClassKodi.SendText2Kodi(s_sendText);
        }

        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {
            WindowSettings windowSettings = new WindowSettings();
            windowSettings.Owner = this;
            windowSettings.ShowDialog();

            if (windowSettings.DialogResult == false) return;

        }

        private void txtSearchString_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            txtSearchString.Text = Clipboard.GetText();
        }
    }
}
