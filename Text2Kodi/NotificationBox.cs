﻿using System.Threading.Tasks;
using System.Windows;

namespace Text2Kodi
{
    class NotificationBox
    {
        private static Window _owner;


        public static MessageBoxResult Show(string text)
        {
            PopupForm(null, text, 2000, NotificationMsg.OK);
            return MessageBoxResult.OK;
        }
        public static MessageBoxResult Show(string text, int delay, NotificationMsg message)
        {
            PopupForm(null, text, delay, message, Position.Center);
            return MessageBoxResult.OK;
        }

        public static MessageBoxResult Show(Window owner, string text, int delay, NotificationMsg message, Position pos)
        {
            _owner = owner;

            PopupForm(_owner, text, delay, message, pos);
            return MessageBoxResult.OK;
        }

        public static async void PopupForm(Window _owner, string label, int delay = 4000, NotificationMsg message = NotificationMsg.OK, Position pos = Position.Center)
        {
            await PopupDelay(_owner, label, delay, message, pos);
        }

        public static async Task PopupDelay(Window _owner, string label, int delay, NotificationMsg message, Position pos)
        {

           WindowNotify box = new WindowNotify( _owner, label, message, pos);

            box.Show();

            await Task.Delay(delay);

            box.Close();
        }



    }

    public enum NotificationMsg
    {
        OK,
        ERROR,
        DONE,
        QUEUE
    }

    public enum Position
    {
        Center,
        Parent
    }


}
