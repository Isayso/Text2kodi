﻿using System.Windows;
using System.Windows.Media;

namespace Text2Kodi
{
    /// <summary>
    /// Interaction logic for WindowNotify.xaml
    /// </summary>
    public partial class WindowNotify : Window
    {
        public WindowNotify(Window _owner, string label, NotificationMsg message, Position pos)
        {
            InitializeComponent();

            Owner = _owner;

            color(message, pos);

            lbl.Text = label;
        }

        public void color(NotificationMsg backgcl, Position screenpos)
        {

            switch (backgcl)
            {
                case NotificationMsg.OK:
                    this.Background = Brushes.DarkGreen;
                    lbl.Foreground = Brushes.White;
                    break;

                case NotificationMsg.ERROR:
                    this.Background = Brushes.DarkRed;
                    lbl.Foreground = Brushes.White;
                    break;

                case NotificationMsg.DONE:
                    this.Background = Brushes.IndianRed;
                    lbl.Foreground = Brushes.White;
                    break;

                case NotificationMsg.QUEUE:
                    this.Background = Brushes.Orange;
                    lbl.Foreground = Brushes.Black;
                    break;

            }

            switch (screenpos)
            {
                case Position.Center:
                    this.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    break;

                case Position.Parent:
                    this.WindowStartupLocation = WindowStartupLocation.Manual;

                    //if (Owner != null)
                    //    Location = new Point(Owner.Location.X + Owner.Width / 2 - Width / 2,
                    //        Owner.Location.Y + Owner.Height / 2 - Height / 2);

                    this.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                    break;
            }


        }

    }
}
