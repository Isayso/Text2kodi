﻿using System.Security.Cryptography;
using System.Text;
using System.Windows;
using Text2Kodi.Properties;

namespace Text2Kodi
{
    /// <summary>
    /// Interaction logic for WindowSettings.xaml
    /// </summary>
    public partial class WindowSettings : Window
    {
        public WindowSettings()
        {
            InitializeComponent();

            ReadSettings();

            if (Settings.Default.cipher != null && Settings.Default.entropy != null)
            {
                byte[] plaintext = ProtectedData.Unprotect(Settings.Default.cipher, Settings.Default.entropy,
                  DataProtectionScope.CurrentUser);
                // string password = PasswordBox.Password;
                password.Password = ByteArrayToString(plaintext);
            }
            else
            {
                password.Password = "";
            }

        }

        private void ReadSettings()
        {
            kodiIP.Text = Settings.Default.rpi;
            port.Text = Settings.Default.port;
            user.Text = Settings.Default.username;

        }

        private void WriteSettings()
        {
            Settings.Default.rpi = kodiIP.Text;
            Settings.Default.port = port.Text;
            Settings.Default.username = user.Text;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            WriteSettings();

            string passtext = password.Password;

            // Data to protect. Convert a string to a byte[] using Encoding.UTF8.GetBytes().
            byte[] plaintext = Encoding.Default.GetBytes(password.Password); ;


            // Generate additional entropy (will be used as the Initialization vector)
            byte[] entropy = new byte[20];
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                rng.GetBytes(entropy);
            }

            byte[] ciphertext = ProtectedData.Protect(plaintext, entropy,
                DataProtectionScope.CurrentUser);

            //https://stackoverflow.com/questions/1766610/how-to-store-int-array-in-application-settings
            Settings.Default.cipher = ciphertext;
            Settings.Default.entropy = entropy;

            Settings.Default.Save();

            this.DialogResult = true;

            this.Close();

        }

        public static string ByteArrayToString(byte[] arr)
        {
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            return enc.GetString(arr);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

            this.Close();

        }
    }
}
